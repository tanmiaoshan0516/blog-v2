<?php

use App\Http\Controllers\PostCommentController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\SessionController;
use App\Models\Post;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/', [PostController::class, 'index'])->name('home');

Route::get('posts/{post:slug}', [PostController::class, 'show']);
Route::post('posts/{post:slug}/comments', [PostCommentController::class, 'store']);

Route::get('register/create', [RegisterController::class, 'create'])->middleware('guest');
Route::post('register/create', [RegisterController::class, 'store'])->middleware('guest');

Route::get('login/index', [SessionController::class, 'create'])->middleware('guest');
Route::post('login/index', [SessionController::class, 'store'])->middleware('guest');
Route::post('logout', [SessionController::class, 'destroy'])->middleware('auth');

Route::get('admin/posts/create', [PostController::class, 'create'])->middleware('admin');
Route::post('admin/posts', [PostController::class, 'store'])->middleware('admin');

Auth::routes();


Route::get('test/{wildcard}', function(){
    $path = __DIR__ . '/./resources/views/test.html';
    // $path = resource_path('/views/test.html');

    // cache
    $variable = cache()->remember("test.{wildcard}", 5, function() use($path) {
        var_dump('refresh cache');
        return file_get_contents($path);
    });

    return view('test', [
        'variable' => $variable
        //question
    ]);
})->where('wildcard', '[A-z]+');

// Route::get('/authors/{author:username}', function (User $author) { 
//     return view('posts.index', [
//         'posts' => $author->posts,
//         'categories' => Category::all()
//     ]);
// });

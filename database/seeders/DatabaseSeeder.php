<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Post::truncate();
        User::truncate();
        Category::truncate();

        // input
        $categoryNum = 8;
        $userNum = 10;
        $postNum = 5000;

        // Create an array to identify the number of posts for each user (total 10 users)
        $arr_post_per_user = array_fill(0,$postNum,1);

        for ($item = 0; $item < ($postNum - $userNum); $item++) {
            // Increment any random element from the array by 1
            $arr_post_per_user[rand(0,$userNum-1)] += 1;
        }

        for ($assignPost = 0; $assignPost < $userNum ; $assignPost++) {
            User::factory()
            ->has(Post::factory()->count($arr_post_per_user[$assignPost]))
            ->create();
        }

        Category::factory()
            ->count($categoryNum)
            ->state(new Sequence(
                ['name' => 'My Personal', 'slug' => 'my-personal'],
                ['name' => 'Business-man', 'slug' => 'business-man'],
                ['name' => 'Professional', 'slug' => 'professional'],
                ['name' => 'Fashion', 'slug' => 'Fashion'],
                ['name' => 'Lifestyle', 'slug' => 'lifestyle'],
                ['name' => 'Travel', 'slug' => 'travel'],
                ['name' => 'Food', 'slug' => 'food'],
                ['name' => 'Affiliate', 'slug' => 'affiliate'],
            ))->create();


        /* Method 1: using factory and sequence with input */

        // input
        // $categoriesArr = ['My Personal', 'Business-man', 'Professional', 'Fashion', 'Lifestyle', 'Travel', 'Food', 'Affiliate'];
        // $categoryNum = count($categoriesArr);
        // $userNum = 10;
        // $postNum = 5000;

        // foreach ($categoriesArr as $category) {
        //     Category::create([
        //         'name' => $category,
        //         'slug' => str_replace(" ", "-", strtolower($category))
        //     ]);
        // }

        // User::factory($userNum)->create();

        // sequence closure will be invoked each time the sequence needs a new value
        // Post::factory($postNum)
        //     ->sequence(fn ($sequence) => [
        //         'user_id' => rand(1, $userNum),
        //         'category_id' => rand(1, $categoryNum),
        //     ])
        //     ->create();

        /* notes: Create Category and assign the name & slug to 'personal' */
        // $personal = Category::create([
        //     'name' => 'Personal',
        //     'slug' => 'personal'
        // ]);

        /* Create Post with 'category_id' = $personal->id */
        // Post::create([
        //     'user_id' => $user->id,
        //     'category_id' => $personal->id,
        //     'slug' => 'my-personal-post',
        //     'title' => 'My Personal Post',
        //     'excerpt' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore',
        //     'body' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore'
        // ]);
    }
}

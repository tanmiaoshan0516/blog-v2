<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SessionController extends Controller
{
    public function create()
    {
        return view('sessions.create');
    }

    public function store()
    {
        $atttibutes = request()->validate([
            'email' => 'required | exists:users,email',
            'password' => 'required'
        ]);

        //attempt to authenticate and log in the user
        // based on the provided credentials
        if (auth()->attempt($atttibutes)) {
            session()->regenerate();

            return redirect('/')->with('success', 'Welcome!');
        }

        //auth failed
        return back()
            ->withInput()
            ->withErrors(['email' => 'Your provided credentials could not be verifed.']);
    }

    public function destroy()
    {
        auth()->logout();

        return redirect('/')->with('success', 'Goodbye!');
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Validation\Rule;

class PostController extends Controller
{
    public function index()
    {
        $keyword = request('search');
        $per_page = request('per_page') ?? 12;

        $posts = Post::latest()->filter(
            request(['search', 'category', 'author'])
        )->paginate($per_page)->withQueryString();

        if ($keyword) {
            $posts->getCollection()->transform(function ($row) use ($keyword) {
                $row->title = preg_replace('/(' . $keyword . ')/i', "<mark>$1</mark>", $row->title);
                $row->excerpt = preg_replace('/(' . $keyword . ')/i', "<mark>$1</mark>", $row->excerpt);
    
                return $row;
            });
        }


        // 'categories' => Category::all(),
        // 'currentCategory' => Category::firstWhere('slug', request('category'))
        
        return view('posts.index', ['posts' => $posts]);
    }

    public function show(Post $post)
    {
        return view('posts.show', [
            'post' => $post
        ]);
    }

    public function create(Post $post)
    {
        return view('posts.create', [
            'post' => $post,
            'categories' => Category::all(),
        ]);
    }

    public function store()
    {
        $attributes =  request()->validate([
            'title' => 'required | min:6',
            'thumbnail' => 'required | image',
            'slug' => 'required',
            'excerpt' => 'required',
            'body' => 'required',
            'category_id' => ['required', Rule::exists('categories', 'id')]
        ]);

        $attributes['user_id'] = auth()->id();
        
        $p = Post::create($attributes);
        $p->thumbnail = request()->file('thumbnail')->store('/' . $p->id, 'thumbnails');
        $p->save();

        return redirect('/');
    }
}

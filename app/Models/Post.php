<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class Post extends Model
{
    use HasFactory;

    protected $guarded = [];
    // protected $fillable = ['category_id', 'slug', 'title', 'excerpt', 'body'];
    protected $with = ['category', 'author'];

    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['search'] ?? false, fn($query, $search) =>
            $query->where(fn($query) => 
                $query->where('title', 'like', '%' .  $search . '%')
                ->orWhere('excerpt', 'like', '%' .  $search . '%')
                // ->orWhere('body', 'like', '%' .  $search . '%')
            )
        );

        $query->when($filters['category'] ?? false, fn($query, $category) =>
            $query->whereHas('category', fn($query) => 
                $query->where('slug', $category)    
            )
        );
        // $query
        //     ->whereExists(fn($query) =>
        //         $query->from('categories')
        //             ->whereColumn('categories.id', 'posts.category_id') // ->whereColumn looking for column
        //             ->where('categories.slug', $category)) //->where looking for specific value = $category
        //     );

        $query->when($filters['author'] ?? false, fn($query, $author) =>
            $query->whereHas('author', fn($query) => 
                $query->where('username', $author)    
            )
        );
    }
    
    public function category() {
        return $this->belongsTo(Category::class);    
    }

    public function author() //referece to user_id the 'user' cannot simply change it's naming
    {
        return $this->belongsTo(User::class, 'user_id'); //unless to state the foreign key
    }

    public function comments() 
    {
        return $this->hasMany(Comment::class);
    }

}


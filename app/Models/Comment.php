<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function author() //referece to user_id the 'user' cannot simply change it's naming
    {
        return $this->belongsTo(User::class, 'user_id'); //unless to state the foreign key
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}

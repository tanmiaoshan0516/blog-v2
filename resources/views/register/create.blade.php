<x-layout>
    <section class="my-6">
        <main class="max-w-xl mx-auto">
            <form action="/register/create" method="POST" class="bg-gray-100 border border-gray-300 p-6 rounded-xl">
                @csrf
                <h1 class="font-bold text-xl text-center mb-8">Register!</h1>
                <div class="my-6">
                    <div class="mb-2">
                        <label for="name" class="uppercase font-bold text-sm">name</label>
                    </div>
                    <div>
                        <input id="name" type="text" name="name" class="w-full px-4 py-2 border border-gray-500" value="{{ old('name') }}" required>
                    </div>

                    @error('name')
                    <div class="text-red-500 text-sm">
                        <p>{{ $message }}</p>
                    </div>
                    @enderror
                </div>

                <div class="my-6">
                    <div class="mb-2">
                        <label for="email" class="uppercase font-bold text-sm">email</label>
                    </div>
                    <div>
                        <input id="email" type="text" name="email" class="w-full px-4 py-2 border border-gray-500" value="{{ old('email') }}" required>
                    </div>

                    @error('email')
                    <div class="text-red-500 text-sm">
                        <p>{{ $message }}</p>
                    </div>
                    @enderror
                </div>

                <div class="my-6">
                    <div class="mb-2">
                        <label for="username" class="uppercase font-bold text-sm">username</label>
                    </div>
                    <div>
                        <input id="username" type="text" name="username" class="w-full px-4 py-2 border border-gray-500" value="{{ old('username') }}" required>
                    </div>
                    
                    @error('username')
                    <div class="text-red-500 text-sm">
                        <p>{{ $message }}</p>
                    </div>
                    @enderror
                </div>

                <div class="my-6">
                    <div class="mb-2">
                        <label for="password" class="uppercase font-bold text-sm">password</label>
                    </div>
                    <div>
                        <input id="password" type="password" name="password" class="w-full px-4 py-2 border border-gray-500" required>
                    </div>
                    
                    @error('password')
                    <div class="text-red-500 text-sm">
                        <p>{{ $message }}</p>
                    </div>
                    @enderror
                </div>

                <div>
                    <button type="submit" class="bg-blue-500 rounded-md px-4 py-2 text-white">Submit</button>
                </div>

            </form>
        </main>
    </section>
</x-layout>
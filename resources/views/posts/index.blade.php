<x-layout>
    @include('posts._header')

    <main class="max-w-6xl mx-auto mt-6 lg:mt-20 space-y-6">

        @if ($posts->count())

        <x-post-featured-card :post="$posts[0]"/>
        <x-posts-grid :posts="$posts->skip(0)"></x-posts-grid>
        {{ $posts->links() }}
        
        @else
        <p class="text-lg text-center">No post.</p>

        @endif

    </main>
</x-layout>
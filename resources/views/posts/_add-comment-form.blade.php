@auth
<x-panel>
    <form action="/posts/{{ $post->slug }}/comments" method="POST">
        @csrf
        <div class="flex flex-col">
            <div class="flex items-center">
                <img src="https://i.pravatar.cc/60?u={{ auth()->id() }}" alt="img" class="rounded-full" width="40" height="40">
                <p class="ml-4 text-md font-semibold">Want to participate?</p>
            </div>
            <div>
                <textarea name="body" id="body" class="border-b border-gray-200 w-full p-4 my-4" cols="30" rows="5" placeholder="Quick, thing of something to say!" required></textarea>
            </div>
            @error('body')
            <div>
                <small class="text-red-500">{{ $message }}</small>
            </div>
            @enderror
            <div class="flex justify-end">
                <x-submit-button>Post</x-submit-button>
            </div>
        </div>
    </form>
</x-panel>
@endauth
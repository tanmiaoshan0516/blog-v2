<x-layout>
    <div class="max-w-lg mx-auto mt-6">
        <h1 class="text-lg font-bold text-center mb-3">Publish New Post</h1>
        <x-panel class="border-gray-300">
            <form action="/admin/posts" method="POST" enctype="multipart/form-data">
                @csrf
                <x-form.input name="title"></x-form.input>
                <x-form.input name="slug"></x-form.input>
                <x-form.input name="excerpt"></x-form.input>
                <x-form.input name="thumbnail" type="file"></x-form.input>
                <x-form.textarea name="body"></x-form.input>
                <div class="mb-6">
                    <div class="mb-2">
                        <label 
                            for="category_id" 
                            class="uppercase font-bold"
                        >
                            category
                        </label>
                    </div>

                    <div>
                        <select name="category_id" id="category_id">
                            @foreach ($categories as $category)
                            <option 
                                value="{{ $category->id }}"
                                {{ old('category_id') == $category->id ? 'selected' : '' }}
                            >{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    @error('body')
                    <p class="text-red-500 text-sx mt-2">{{ $message }}</p>
                    @enderror
                </div>

                <div>
                    <x-submit-button>publish</x-submit-button>
                </div>
            </form>
        </x-panel>
    </div>
</x-layout>
@include('components.layout')
<body class="mt-6">
    <h2>{{ $post->title }}</h2>
    <p>
        By <a href="/?author={{$post->author->username}}">{{ $post->author->name }}</a>
    </p>
    <a href="/?category={{$post->category->slug}}">{{ $post->category->name }}</a>
    <div>
        {!! $post->body !!}
    </div>
    <a href="/">Back</a>
</body>


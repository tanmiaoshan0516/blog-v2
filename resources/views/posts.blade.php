@extends('components.layout')

<div class="relative">
    <div class="fixed top-0 left-0 right-0 mx-6 flex">
        <div>
            Category:
            @foreach ($categories as $category)
            <!-- class="{{ request()->is('categories/' . $category->slug) ? 'font-bold' : '' }}" -->
            <a class="{{ request()->is('categories/'. $category->slug) ? 'font-bold' : '' }}" href="/?category={{ $category->slug }}&{{ http_build_query(request()->except('category', 'page')) }}">{{ ucwords($category->name) }}</a>
            @endforeach
        </div>
        <div class="mx-4">
            <form action="/" method="GET">
                @if (request('category'))
                <input type="hidden" name="category" value="{{ request('category') }}">
                @endif

                <input type="text" name="search" placeholder="Search" value="{{ request('search') }}">
            </form>
        </div>
    </div>
</div>

@section('body')
<div class="max-w-lg m-auto mt-6">
    {{ isset($çurrentCategory) ? 'Category: ' . ucwords($çurrentCategory->name) : '' }}
    @if ($posts->count())
    @foreach ($posts as $post)
    <article class="my-4">
        <h1>
            Title:
            <a href="/posts/{{$post->slug}}">
                {{ $post->title }}
            </a>
        </h1>
        <p>
            By <a href="/?author={{$post->author->username}}">{{ $post->author->name }}</a>
        </p>
        <p>
            Category:
            <a href="/?category={{$post->category->slug}}">{{ $post->category->name }}</a>
        </p>
        <div>
            Excerpt: {!! $post->excerpt !!}
        </div>
    </article>
    @endforeach
    {{ $posts->links() }}
    @else
    <p>No post.</p>
    @endif
</div>
@endsection
@props(['comment'])
<x-panel class="flex bg-gray-100">
    <div class="mr-4 flex-shrink-0">
        <img src="https://i.pravatar.cc/60?u={{ $comment->author->id }}" alt="img" class="rounded-xl" width="60" height="60">
    </div>
    <div class="flex flex-col">
        <p class="text-lg font-bold">{{ $comment->author->username }}</p>
        <time class="text-xs">Posted {{ $comment->created_at->diffForHumans() }}</time>
        <p class="mt-4">
            {{ $comment->body }}
        </p>
    </div>
</x-panel>
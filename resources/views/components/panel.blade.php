<div {{ $attributes(['class' => 'border border-gray-200 p-6 rounded-xl my-6']) }}>
    {{ $slot }}
</div>
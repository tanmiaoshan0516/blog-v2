<button 
    class="uppercase bg-blue-500 rounded-full px-8 py-2 text-sm text-white hover:bg-blue-600"
>{{ $slot }}
</button>
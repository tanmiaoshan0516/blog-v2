@props(['name'])

<div class="mb-6">
    <div class="mb-2">
        <label for="{{ $name }}" class="uppercase font-bold">
            {{ $name }}
        </label>
    </div>

    <div>
        <textarea id="{{ $name }}" name="{{ $name }}" class="border border-gray-300 w-full p-2" required>{{ old($name, '') }}</textarea>
    </div>
    <x-form.error name="{{ $name }}"></x-form.error>
</div>
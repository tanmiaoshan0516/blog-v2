@props(['name', 'type' => 'text'])
<div class="mb-6">
    <div class="mb-2">
        <label for="{{ $name }}" class="uppercase font-bold">
            {{ ucwords($name) }}
        </label>
    </div>

    <div>
        <input id="{{ $name }}" type="{{ $type }}" class="border border-gray-300 w-full p-2" name="{{ $name }}" value="{{ old($name, '') }}" class="px-3 py-1 w-full" required>
    </div>

    <x-form.error name="{{ $name }}"></x-form.error>
</div>